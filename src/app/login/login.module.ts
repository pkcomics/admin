import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule, 
        TranslateModule, 
        ReactiveFormsModule,
        FormsModule,
        NgbModule,
        LoginRoutingModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
