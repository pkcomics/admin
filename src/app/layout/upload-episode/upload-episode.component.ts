import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UploadService } from '../../shared/services/upload.service';
import { tap } from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../environments/environment.prod';
import { ToastrService } from 'ngx-toastr';
import { GetService } from '../../shared/services/get.service';
import * as moment from 'moment';
import { NgbDateCustomParserService } from '../../shared/services/ngb-date-custom-parser.service';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';

const URL = environment.url;
@Component({
  selector: 'app-upload-episode',
  templateUrl: './upload-episode.component.html',
  styleUrls: ['./upload-episode.component.scss'],
})
export class UploadEpisodeComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({
    url: `${URL}/upload-episode`,
    itemAlias: 'thumb'
  });

  form: FormGroup;
  loading = false;
  comics: any;
  isDisabled: boolean = true;

  result;

  constructor(
    private fb: FormBuilder,
    private uploadService: UploadService,
    private getService: GetService,
    private toastr: ToastrService,
    private ngbDateParserFormatter: NgbDateCustomParserService

  ) {

    this.form = this.fb.group({
      comic_id : ['', Validators.required],
      name : [{ value: '', disabled: this.isDisabled }, Validators.required],
      index_n: ['', Validators.required],
      reg_date: [''],
    });
   }

  ngOnInit(): void {
    this.getService.seriesList().subscribe((res: any) => {
      this.comics = res;
    });

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
      this.toastr.success('File successfully uploaded!');
    };
  }

  onSubmit(){
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    console.log(this.form.value);
    /*
    return this.uploadService.episode(this.form.value)
    .pipe(tap(
      res => {
        this.result = res;
        this.loading = false;
      }));
    */
  }

  seriesActive() {
    this.isDisabled = false;
    this.form.controls['name'].enable();

  }

}
