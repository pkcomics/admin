import { FileUploadModule } from 'ng2-file-upload';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadEpisodeComponent } from './upload-episode.component';
import { PageHeaderModule } from './../../shared';
import { UploadEpisodeRoutingModule } from './upload-episode-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UploadEpisodeComponent,
  ],
  imports: [
    CommonModule, 
    UploadEpisodeRoutingModule, 
    PageHeaderModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],

})
export class UploadEpisodeModule { }
