import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadEpisodeComponent } from './upload-episode.component';

describe('UploadEpisodeComponent', () => {
  let component: UploadEpisodeComponent;
  let fixture: ComponentFixture<UploadEpisodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadEpisodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadEpisodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
