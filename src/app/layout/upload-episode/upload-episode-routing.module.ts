import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadEpisodeComponent } from './upload-episode.component';

const routes: Routes = [
    {
        path: '',
        component: UploadEpisodeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadEpisodeRoutingModule {}
