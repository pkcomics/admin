import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadContentsRoutingModule } from './upload-contents-routing.module';
import { PageHeaderModule } from '../../shared';
import { UploadContentsComponent } from './upload-contents.component';
import { FileDropDirective, FileSelectDirective, FileUploader, FileUploadModule } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UploadContentsComponent,
  ],
  imports: [
    CommonModule,
    UploadContentsRoutingModule,
    PageHeaderModule,
    FormsModule,
    FileUploadModule
  ]
})
export class UploadContentsModule { }
