import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadContentsComponent } from './upload-contents.component';

const routes: Routes = [
    {
        path: '',
        component: UploadContentsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadContentsRoutingModule {}
