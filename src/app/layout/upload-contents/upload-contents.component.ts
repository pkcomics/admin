import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../environments/environment.prod';

const URL = environment.url;
@Component({
  selector: 'app-upload-contents',
  templateUrl: './upload-contents.component.html',
  styleUrls: ['./upload-contents.component.scss']
})
export class UploadContentsComponent implements OnInit {
  uploader:FileUploader;
  hasBaseDropZoneOver:boolean;
  response:string;

  constructor() {
    this.uploader = new FileUploader({
      url: `${URL}/upload-contents`,
      formatDataFunction: async (item) => {
        return new Promise( (resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item._file.type,
            date: new Date()
          });
        });
      }
    });
 
    this.hasBaseDropZoneOver = false;
 
    this.response = '';
 
    this.uploader.response.subscribe( res => this.response = res );
   }

  ngOnInit(): void {
    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('someField', 'ggg');
     };
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 

}
