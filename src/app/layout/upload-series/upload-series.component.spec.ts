import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadSeriesComponent } from './upload-series.component';

describe('UploadSeriesComponent', () => {
  let component: UploadSeriesComponent;
  let fixture: ComponentFixture<UploadSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
