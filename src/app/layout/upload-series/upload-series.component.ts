import { environment } from './../../../environments/environment.prod';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';

const URL = environment.url;

@Component({
  selector: 'app-upload-series',
  templateUrl: './upload-series.component.html',
  styleUrls: ['./upload-series.component.scss']
})
export class UploadSeriesComponent implements OnInit {
  uploader:FileUploader;
  form: FormGroup;
  
  constructor(
    private el: ElementRef,
    private fb: FormBuilder,
  ) { 
    this.form = this.fb.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      urlPath: ['', Validators.required],
      description: ['', Validators.required],
    });

    this.uploader = new FileUploader({
      url: `${URL}/upload-series`
    });

    this.uploader.response.subscribe( res => console.log(res) );

  }

  ngOnInit(): void {

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('title', this.form.controls['title'].value);
      form.append('author', this.form.controls['author'].value);
      form.append('urlPath', this.form.controls['urlPath'].value);
      form.append('description', this.form.controls['description'].value);
      form.append('thumbUrl', this.form.controls['thumbUrl'].value);
      form.append('backdropUrl', this.form.controls['backdropUrl'].value);

    };
  }


  onSubmit(){
    if (this.form.invalid) {
      return;
    }
 
    let thumbEl: HTMLInputElement =  this.el.nativeElement.querySelector('#thumb');
    let backdropEl: HTMLInputElement =  this.el.nativeElement.querySelector('#backdrop');

    this.form.addControl('thumbUrl', new FormControl(thumbEl.files.item(0).name, Validators.required));
    this.form.addControl('backdropUrl', new FormControl(backdropEl.files.item(0).name, Validators.required));

    this.uploader.uploadAll();
  }

}
