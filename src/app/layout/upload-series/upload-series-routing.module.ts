import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadSeriesComponent } from './upload-series.component';

const routes: Routes = [
    {
        path: '',
        component: UploadSeriesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadSeriesRoutingModule {}
