import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadSeriesComponent } from './upload-series.component';
import { PageHeaderModule } from './../../shared';
import { UploadSeriesRoutingModule } from './upload-series-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  declarations: [UploadSeriesComponent],
  imports: [
    CommonModule, 
    UploadSeriesRoutingModule, 
    PageHeaderModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
    
})
export class UploadSeriesModule { }
