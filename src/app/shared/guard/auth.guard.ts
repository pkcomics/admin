import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private auth: AuthService) {}

    canActivate() {
        if (!this.auth.isAuthenticated()) {
            console.log('invalid token!');
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}
