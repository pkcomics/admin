import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable()
export class UploadService {
  constructor (
    private apiService: ApiService
  ) {}


  episode(body): Observable<any> {
    return this.apiService.post('/uploadEpisode', body);
  }

  series(body): Observable<any> {
    return this.apiService.post('/uploadSeries', body);
  }


}
