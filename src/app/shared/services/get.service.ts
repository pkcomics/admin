import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable()
export class GetService {
  constructor (
    private apiService: ApiService
  ) {}

  seriesList(): Observable<any> {
    return this.apiService.get('/getSeries');
  }

}
