import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment.prod';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = environment.url;
  public headers: any;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router

  ) {
    this.headers = new HttpHeaders()
    .set('Authorization', 'Bearer ' + this.auth.getToken());
  }

  private formatErrors(e: any) {
    return throwError(e.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${this.url}${path}`, { params })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${this.url}${path}`,
      JSON.stringify(body)
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object= {}): Observable<any> {
    return this.http.post(
      `${this.url}${path}`,
      JSON.stringify(body)
      ).pipe(this.formatErrors);
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${this.url}${path}`
    ).pipe(catchError(this.formatErrors));
  }

}


