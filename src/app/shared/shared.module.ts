import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ApiService,
  AuthService,
  UploadService,
  GetService,
} from './services';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ApiService,
    AuthService,
    UploadService,
    GetService,
  ],
  declarations: []
})
export class SharedModule { }
