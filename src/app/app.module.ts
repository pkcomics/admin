import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';
import { MaterialModule } from './shared/material/material.module';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from './shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        MaterialModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-right',
            preventDuplicates: true,
        }), // ToastrModule added
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        SharedModule,
    ],
    declarations: [
        AppComponent,
    ],
    providers: [AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {}
